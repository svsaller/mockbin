# Mockbin 

New Mockbin fork, without the limitation of 100 entries per bin.

[Here is the original README of Mashape.](/README_Mashape.md)

![Docker][docker-logo]

## Install with [Docker](https://www.docker.com/)

### Building the docker image

```shell
docker build -t mockbin .
```

### Running the docker container

To run, this image needs to be linked to a Redis container:

```shell
docker run -d --name mockbin_redis redis
docker run -d -p 8080:8080 --link mockbin_redis:redis mashape/mockbin
```

### Run with docker-compose

There is also a docker-compose file in the root directory, that does all the work...

```shell
docker-compose up
```

(Per default the mockbin service runs on port 8080.)

## License

[MIT](LICENSE) &copy; [Mashape](https://www.mashape.com)

[docker-logo]: https://d3oypxn00j2a10.cloudfront.net/0.16.0/images/pages/brand_guidelines/small_h.png